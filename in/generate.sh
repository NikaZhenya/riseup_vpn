pc-automata -f ../data/md/riseup_vpn.md \
            -c ../data/img/portada.png \
            -s ../data/css/styles.css \
            -i ../data/img/ \
            --overwrite

mv epub*3-0-0* ../out/epub.epub
mv mobi-* ../out/mobi.mobi
rm epub-*

pc-pandog -i ../data/md/riseup_vpn.md \
          -o ../index.html

sed -i 's/\.\.\/img/.\/data\/img/g' ../index.html
sed -i 's/<title>Título/<title>RiseupVPN en OpenWrt con OpenVPN/g' ../index.html
