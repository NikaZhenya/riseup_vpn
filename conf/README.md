# Archivo de configuración

Para ciertos clientes es necesario ingresar un archivo de
configuración. Para sistemas UNIX la extensión suele ser `.conf`.
Para Windows suele tener la extensión `.ovpn`.

## Personalización

1. Cambia el nombre de extensión según el sistema operativo.
2. Guarda un archivo `auth.txt` donde la primera línea es tu
   usuario sin `@riseup.net` y la segunda, tu contraseña.
3. Modifica la ruta a `auth.txt` en la línea 5 de este archivo
   de configuración.
4. ¡Listo!
