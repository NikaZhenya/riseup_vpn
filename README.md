# RiseupVPN en OpenWrt con OpenVPN

Este tutorial es para configurar RiseupVPN en un _router_ con
OpenWrt y OpenVPN.

Esto permite navegar a través de una VPN en cualquier dispositivo
que esté conectado al _router_ sin necesidad de instalar _software_
adicional.

Es decir, brinda mayor privacidad al acceder a internet porque
se usa un «túnel» donde tu información es inaccesible para tu
proveedor de internet.

# Configuración automatizada

En el directorio `sh` se encuentra un _script_ que automatiza la
configuración.

Uso:

```
sh install.sh [-u usuario] [-p contraseña]
```

OJO: el usuario y contraseña corresponde a tu cuenta de Riseup. Ve los
puntos __a—c__ del tutorial.

## Archivos

* [GitLab](https://gitlab.com/NikaZhenya/riseup_vpn)
* [GitHub](https://github.com/NikaZhenya/riseup_vpn)
* [EPUB](https://gitlab.com/NikaZhenya/riseup_vpn/raw/master/out/epub.epub)
* [MOBI](https://gitlab.com/NikaZhenya/riseup_vpn/raw/master/out/mobi.mobi)

## Sitio

Puedes leer esta documentación [aquí](https://nikazhenya.gitlab.io/riseup_vpn).
